import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { HttpErrorResponse } from '@angular/common/http';

declare var jQuery: any;
@Component({
  selector: 'app-show-employees',
  templateUrl: './show-employees.component.html',
  styleUrls: ['./show-employees.component.css']
})
export class ShowEmployeesComponent implements OnInit {
  
  employees: any;
  emailId: any;
  countries: any;
  departments: any;
  editEmp: any;       //for 2-way databinding with dialog box

  //Dependency Injection for EmpService
  constructor(private service: EmpService) {
    this.emailId = localStorage.getItem('emailId');
    this.editEmp = {
      empId: '',
      empName: '',
      salary: '',
      gender: '',
      doj: '',
      country: '',
      emailId: '',
      password: '',
      department: {
        deptId: ''
      }
    };

  }

  ngOnInit() {
    this.service.getAllEmployees().subscribe((data: any) =>{this.employees = data; } );
    this.service.getAllCountries().subscribe((data: any) => { this.countries = data; });
    this.service.getAllDepartments().subscribe((data: any) => { this.departments = data; });
  }

  editEmployee(emp: any) {
    console.log(emp);
    jQuery('#myModal').modal('show');

  }
  updateEmployee() {
    console.log(this.editEmp);
    this.service.updateEmployee(this.editEmp).subscribe((data: any) => { console.log(data); });
  }


  deleteEmployee(emp: any) {
    this.service.deleteEmployee(emp.empId).subscribe((data: any) => {console.log(data);});

    const i = this.employees.findIndex((element: any) => {
      return element.empId == emp.empId;
    });

    this.employees.splice(i, 1);

    alert('Employee Deleted Successfully!!!');
  }
}

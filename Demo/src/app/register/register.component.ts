import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {
  
  countries: any;
  departments: any;
  emp: any;
  captchaResponse: string = '';
  toastr: any;
  password:string='';
  confirmPassword: string = '';
  emailId:string ='';
  phoneNumber:string='';
  doj:string='';
  gender:string='';
  salary:string='';
  empName:string='';


  constructor(private service: EmpService, private router: Router) {
    this.emp = {
      empName:'',
      salary:'',
      gender:'',
      doj:'',
      country:'',
      emailId:'',
      password:'',
      confirmPassword: '',
      phonenumber:'',
      department: {
        deptId:''
      }
    };
  }

  ngOnInit() {
    this.service.getAllCountries().subscribe((data: any) => {this.countries = data;});
    this.service.getAllDepartments().subscribe((data: any) => {this.departments = data;});
  }
 

  registerSubmit(regForm: any) {
    console.log('Register form submitted:', regForm);
    console.log('Passwords:', this.emp.password, this.confirmPassword);
    console.log(regForm);
    if (this.emp.password !== this.confirmPassword) {
      alert('Passwords do not match.');
      return;
    }
    // Validate captcha
    if (!this.captchaResponse) {
      alert('Please complete the captcha.');
      return;
    }

    this.emp.empName = regForm.empName;
    this.emp.salary = regForm.salary;
    this.emp.gender = regForm.gender;
    this.emp.doj = regForm.doj;
    this.emp.country = regForm.country;
    this.emp.emailId = regForm.emailId;
    this.emp.password = regForm.password;
    this.emp.phonenumber = regForm.phonenumber;
    this.emp.department.deptId = regForm.department;

    this.service.regsiterEmployee({...this.emp, captchaResponse: this.captchaResponse })
      .subscribe((data: any) => {
        console.log(data);
        // Handle success, e.g., show a success message
        this.toastr.success('Registration successful', 'Success');
        this.router.navigate(['login']);
      });
  }

  handleCaptchaResponse(captchaResponse: string) {
    this.captchaResponse = captchaResponse;
  }
}
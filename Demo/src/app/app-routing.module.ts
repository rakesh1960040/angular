import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

import { ShowempbyidComponent } from './showempbyid/showempbyid.component';
import { ProductComponent } from './product/product.component';
import { LogoutComponent } from './logout/logout.component';

import { authGuard } from './auth.guard';
import { CartComponent } from './cart/cart.component';
import { ShowEmployeesComponent } from './show-employees/show-employees.component';


const routes: Routes = [
  {path:'', component:LoginComponent},
  {path:'login', component:LoginComponent},
  {path:'register', component:RegisterComponent},
  {path:'showemps', component:ShowEmployeesComponent},
  {path:'showempbyid',  canActivate:[authGuard],component:ShowempbyidComponent},
  {path:'product', canActivate:[authGuard], component:ProductComponent},
  {path:'logout', canActivate:[authGuard],component:LogoutComponent},
  { path: '', redirectTo: '/products', pathMatch: 'full' },
  {path:'cart', component:CartComponent},

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

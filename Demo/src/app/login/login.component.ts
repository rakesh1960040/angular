// login.component.ts

import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  emailId: string = '';
  password: string = '';
  employees: any[] = [];
  emp: any;
  captchaResponse: string = '';

  constructor(private router: Router, private toastr: ToastrService, private service: EmpService) { }

  ngOnInit() {
    // Fetch employee data when component initializes
    this.service.getAllEmployees().subscribe((data: any) => {
      this.employees = data;
    });
  }

  loginSubmit(loginForm: any) {
    if (loginForm.emailId === 'HR' && loginForm.password === 'HR') {
      this.service.setIsUserLoggedIn();
      localStorage.setItem("emailId", loginForm.emailId);
      this.router.navigate(['showemps']);
    } else {
      this.emp = this.employees.find((element: any) => element.emailId === loginForm.emailId && element.password === loginForm.password);

      if (this.emp) {
        this.service.setIsUserLoggedIn();
        localStorage.setItem("emailId", loginForm.emailId);
        this.router.navigate(['product']);
      } else {
        this.toastr.error('Invalid Credentials', 'Error', {
          closeButton: true,
          progressBar: true,
          positionClass: 'toast-top-right',
          tapToDismiss: false,
          timeOut: 3000,
        });
        return;
      }
    }

    this.toastr.success('Login Successful!', 'Success', {
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-top-right',
      tapToDismiss: false,
      timeOut: 3000,
    });
  }

  handleCaptchaResponse(captchaResponse: string) {
    this.captchaResponse = captchaResponse;
  }
}

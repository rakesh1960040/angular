import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {

  id: number;
  name: string;
  avg: number;
  address: any;
  hobbies: any;

  constructor(){
  //  alert("comstructor invoked...")
    this.id = 101;
    this.name = 'Rakesh';
    this.avg = 45.88;

    this.address = {
       streetNo: 101,
       city: 'hyderbad',
       state: 'telangana'
    };

    this.hobbies  = ['Gameing', 'Eating']
    
   }
  ngOnInit(){
   // alert("ngOnInit invoked...")
  }
    
  

}
